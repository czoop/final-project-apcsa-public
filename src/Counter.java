import java.util.ArrayList;

/**
 * Used to control the timing of certain things proportional to
 * render speed. Mainly used to slow the movement of enemies
 * @author Connor Zwick
 *
 */
public class Counter 
{
	private int max;
	private int currentVal;
	
	/**
	 * Creates a new counter object
	 * @param max The max value of the counter before it
	 * returns true and resets
	 */
	public Counter(int max)
	{
		this.max = max;
		this.currentVal = 0;
	}
	
	/**
	 * Checks if the counter has reached the max value, if not,
	 * it increases the counter by one.
	 * @return True if the counter has reached or exceeded its max value,
	 * false otherwise.
	 */
	public boolean ready()
	{
		if (currentVal >= max)
		{
			currentVal = 0;
			return true;
		}
		else
		{
			currentVal++;
			return false;
		}
	}
}
