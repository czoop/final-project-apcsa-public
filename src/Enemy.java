
public class Enemy extends Player implements Intelligent
{
	protected Player player;
	
	//Image Positions
	private int sx;
	private int sy;
	private int sw;
	private int sh;
	
	/**
	 * Creates a new enemy. Sprites and health are chosen based off
	 * of the player's current level
	 * @param view The view controller used by the game
	 */
	public Enemy(View view)
	{
		super(view);
		this.dungeon = view.dungeon();
		this.player = view.hero();
		this.lives = 5;
		
		double rand = Math.random();
		double pl = (double) (view.hero().getLevel());
		if (0.01*pl > rand)
		{
			//Giant
			double r = Math.random();
			if (r < 1.0/3)
			{
				//Green Pig
				sx = 100;
				sy = 180;
				sw = 28;
				sh = 30;
			}
			else if (r < 2.0/3)
			{
				//Fuzzy
				sx = 128;
				sy = 176;
				sw = 28;
				sh = 34;
			}
			else 
			{
				//Goldilocks
				sx = 160;
				sy = 178;
				sw = 32;
				sh = 32;
			}
			this.lives = 30;
		}
		else if (0.03*pl + 0.01*pl > rand)
		{
			//Sorcerer
			double r = Math.random();
			if (r < 1.0/3)
			{	
				//Red Sorcerer
				sx = 80;
				sy = 175;
				sw = 16;
				sh = 18;
				this.lives = 20;
			}
			else if (r < 2.0/3)
			{
				//Necromancer
				sx = 64;
				sy = 176;
				sw = 14;
				sh = 16;
			}
			else
			{
				//Chompy
				sx = 48;
				sy = 176;
				sw = 14;
				sh = 16;
			}
		}
		else if (0.05*pl + 0.03*pl + 0.01*pl > rand)
		{
			//Goblin
			sx = 32;
			sy = 160;
			sw = 16;
			sh = 16;
			this.lives = 10;
		}
		else
		{
			//Fire Monster
			sx = 32; 
			sy = 192;
			sw = 16;
			sh = 16;
		}
	}
	
	/**
	 * Causes the enemy to move when they are in view of, or around
	 * the player. Enemies will act differently based off of their
	 * proximity to the player
	 */
	public void ai()
	{
		int searchDistance = 15;
		int distanceFromPlayer = distanceFrom(player);
		if (distanceFromPlayer <= searchDistance)
		{
			Direction d = directionToTravel(player);
			//System.out.println(d);
			switch (d)
			{
				case NE:
					moveUp();
					moveRight();
					break;
				case NW:
					moveUp();
					moveLeft();
					break;
				case SE:
					moveDown();
					moveRight();
					break;
				case SW:
					moveDown();
					moveLeft();
					break;
				case UP:
					moveUp();
					break;
				case DOWN:
					moveDown();
					break;
				case RIGHT:
					moveRight();
					break;
				case LEFT:
					moveLeft();
					break;
				default:
					break;
			}
		}
		
		if (distanceFromPlayer < 50)
		{
			double rand = Math.random();
			if (rand < 0.25)
			{
				moveUp();
			}
			else if (rand < 0.5)
			{
				moveDown();
			}
			else if (rand < 0.75)
			{
				moveLeft();
			}
			else
			{
				moveRight();
			}
		}
	}
	
	/**
	 * A method that allows the player to harm the enemy. 
	 * A slash subtracts from the health of the enemy.
	 * Returns true if the enemy dies
	 * @return True if the enemy is out of life, false otherwise.
	 */
	public boolean slash()
	{
		if (lives >= 0)
		{
			lives--;
			view.hero().addXP(1);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * Causes the monster to move to a given position
	 */
	@Override
	protected void move(int x, int y)
	{
		if (dungeon.isValidPosition(x, y))
		{
			this.x = x;
			this.y = y;
		}
	}
	
	/**
	 * Draws the monster and its given sprite on screen
	 */
	@Override 
	public void draw()
	{
		double scale = view.getScale();
		double dx = (x - view.getX()) * scale;
		double dy = (y - view.getY()) * scale;
		if (!facingRight)
		{
			//view.gc().drawImage(view.getSprites(), sx, sy, sw, sh, dx, dy, scale, scale);
			view.gc().drawImage(view.getSprites(), sx, sy, sw, sh, dx-((sw-16)/2), dy, sw, sh);
			
		}
		else
		{
			//view.gc().drawImage(view.getSprites(), sx + sw, sy, -sw, sh, dx, dy, scale, scale);
			view.gc().drawImage(view.getSprites(), sx + sw, sy, -sw, sh, dx-((sw-16)/2), dy, sw, sh);
		}
		
//		if (facingRight)
//		{
//			view.gc().drawImage(view.getSprites(), 240, 230, 16.0, 26.0, dx, dy - 3, scale, scale + 10);
//		}
//		else //FacingLeft
//		{
//			view.gc().drawImage(view.getSprites(), 240 + 16, 230, -16.0, 26.0, dx, dy - 3, scale, scale + 10);
//		}
	}
}
