import javafx.scene.paint.Color;

public class Background extends GameObject 
{
	private Color color;
	
	/**
	 * Creates a new background object
	 * @param view The view controller for the game.
	 */
	public Background(View view)
	{
		super(view);
		color = Color.BLACK;
	}
	
	/**
	 * Draws the black background
	 */
	@Override
	public void draw()
	{
		view.gc().setFill(color);
		view.gc().fillRect(0, 0, view.getWindowWidth(), view.getWindowHeight());
	}
}
