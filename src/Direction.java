/**
 * Used to indicate a desired direction for players and enemies.
 * Key to movement of game objects
 * @author Connor Zwick
 */
public enum Direction {
	//Key: NW = North West or Up and to the left
	UP, DOWN, LEFT, RIGHT, NW, NE, SW, SE
}
