import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Message extends GameObject
{
	private String message;
	private boolean showing;
	
	/**
	 * Creates a new Message. Messages can be spawned during 
	 * interactions with certain objects 
	 * and can be dismissed with the space bar
	 * @param view The view controller for the game
	 */
	public Message(View view)
	{
		super(view);
	}
	
	/**
	 * Changes the message displayed to the player
	 * @param str The message to be displayer (make sure it's not too long!)
	 */
	public void setMessage(String str)
	{
		this.message = str;
	}
	
	/**
	 * Tell the message to appear on the next call to draw()
	 */
	public void showMessage()
	{
		showing = true;
	}
	
	/**
	 * Tell the message to disappear on the next call to draw()
	 */
	public void hideMessage()
	{
		showing = false;
	}
	
	/**
	 * Draws the message
	 */
	@Override
	public void draw()
	{
		if (showing)
		{
			int w = view.getWindowWidth() - 20;
			int h = view.getWindowHeight()/4;
			int x = 10;
			int y = (3*view.getWindowHeight())/4;
			view.gc().setFill(Color.rgb(10, 10, 10, 0.5));
			view.gc().fillRoundRect(x, y, w, h, 30, 30);
			
			//Draws Message
			Font defaultFont = view.gc().getFont();
			view.gc().setFont(new Font("Lato Bold", 24));
			view.gc().setFill(Color.WHITE);
			view.gc().fillText(message, x + 30, y + 30, w);
			
			//Draws the Controls Message
			view.gc().setFont(new Font("Lato Bold", 14));
			view.gc().setFill(Color.CADETBLUE);
			view.gc().fillText("Left Click to Attack, Right Click to Place a Bomb", x + 30, y + 50, w);
			view.gc().fillText("Use the arrow keys or WASD to move", x + 30, y + 70, w);
			
			//Draws the press space to continue message
			view.gc().setFill(Color.LIGHTBLUE);
			view.gc().fillText("Press Space To Continue", x + 30, y + 90, w);
			
			//int width = g.getFontMetrics().stringWidth(text);
			
			view.gc().setFont(defaultFont);
		}
	}
}
