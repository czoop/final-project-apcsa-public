/**
 * Game Objects are used in the world array list and are rendered
 * on screen
 * @author Connor Zwick
 */
public abstract class GameObject 
{
	protected int x;
	protected int y;

	protected View view;
	
	/**
	 * Constructs a new game object requiring a reference to the
	 * game's view controller
	 * @param view The view controller used by the game
	 */
	public GameObject(View view)
	{
		this.view = view;
	}
	
	/**
	 * A draw method to be overridden by extensions to
	 * the Game Object class
	 */
	public void draw()
	{
		
	}

	/**
	 * Calculates the distance from this object to the given object.
	 * This is the radial distance approximated using the pythagorean
	 * theorem.
	 * @param go The game object to be referenced
	 * @return The radial distance from the given game object to this
	 */
	public int distanceFrom(GameObject go)
	{
		//Uses pythag theorem to get a radius
		int xDist = Math.abs(this.x - go.getX());
		int yDist = Math.abs(this.y - go.getY());
		return (int) Math.sqrt((xDist*xDist) + (yDist*yDist));
	}
	
	/**
	 * Returns the direction this game object must travel in in order
	 * to intersect the given game object
	 * @param go The game object to be referenced
	 * @return The Direction this must travel in in order to intersect
	 * the given game object
	 */
	public Direction directionToTravel(GameObject go)
	{
		int xDist = go.getX() - this.x;
		int yDist = go.getY() - this.y;
		
		yDist = -yDist;
		
//		int xDist = this.x - go.getX();
//		int yDist = this.y - go.getY();
		
		if (xDist > 0 && yDist > 0)
		{
			//First Quadrant
			return Direction.NE;
		}
		if (xDist < 0 && yDist > 0)
		{
			//Second Quadrant
			return Direction.NW;
		}
		if (xDist > 0 && yDist < 0)
		{
			//Third Quadrant
			return Direction.SE;
		}
		if (xDist < 0 && yDist < 0)
		{
			//Fourth Quadrant
			return Direction.SW;
		}
		
		//Straight up or down
		if (xDist == 0 && yDist > 0)
		{
			return Direction.UP;
		}
		if (xDist == 0 && yDist < 0)
		{
			return Direction.DOWN;
		}
		
		//Left and Right
		if (xDist > 0 && yDist == 0)
		{
			return Direction.RIGHT;
		}
		if (xDist < 0 && yDist == 0)
		{
			return Direction.LEFT;
		}
		
		//If none of the above are true, return a random direction
		int r = (int) (Math.random()*4);
		switch (r)
		{
			case 0:
				return Direction.NE;
			case 1:
				return Direction.NW;
			case 2:
				return Direction.SE;
			case 3:
				return Direction.SW;
		}
		
		//Since the function has to return something!
		return Direction.UP;
	}
	
	/**
	 * Returns the current x position of the game object
	 * @return The x position of the game object
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x position of the game object
	 * @param x The x position to be used by the game object
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Returns the y position of the game object
	 * @return The y position of the game object
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y position of the game object
	 * @param y The y position to be used by the game object
	 */
	public void setY(int y) {
		this.y = y;
	}
	
//	public boolean onScreen()
//	{
//		if ((x1 < view.getX() + view.getWidth() ||
//			 x2 < view.getX() + view.getWidth()) &&
//			(y1 < view.getY() + view.getHeight() ||
//			 y2 < view.getY() + view.getHeight()))
//		{
//		return true;
//		}
//		else
//		{
//			return false;
//		}
//
//	}
}
