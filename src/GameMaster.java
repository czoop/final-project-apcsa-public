import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
 
public class GameMaster extends Application
{
	private static final int WINDOW_WIDTH = 800;
	private static final int WINDOW_HEIGHT = 800;
	
	private static final int VIEW_WIDTH = 50;
	private static final int VIEW_HEIGHT = 50;
	
	private int worldWidth;
	private int worldHeight;
	
	private View view;
	private ArrayList<Direction> direction;
	private ArrayList<GameObject> world;
	
	private GraphicsContext gc;
	private Canvas canvas;
	/**
	 * The runner used to start the game
	 * @param args 
	 */
    public static void main(String[] args) 
    {
    	//Launches Canvas
        launch(args);
    }
    
    /**
     * Called to begin the JavaFX's creation of the canvas
     * Creates the world and sets up event handlers
     */
    @Override
    public void start(Stage primaryStage) 
    {
	primaryStage.setTitle("Quest!");
	Group root = new Group();
	canvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
	gc = canvas.getGraphicsContext2D();
	root.getChildren().add(canvas);
	primaryStage.setScene(new Scene(root));
	primaryStage.show();
	 
	startGame();
	listen(primaryStage);
    }
    
    /**
     * Initializes critical values needed to play the game 
     * and call necessary methods to ready the dungeon.
     */
    private void startGame()
    {
    	worldWidth = 1000;
    	worldHeight = 1000;
    	
    	direction = new ArrayList<Direction>();
    	world = new ArrayList<GameObject>();
    	
    	view = new View(0, 0, VIEW_WIDTH, VIEW_HEIGHT, 
    					worldWidth, worldHeight, 
    					WINDOW_WIDTH, WINDOW_HEIGHT, gc);
    	
    	createWorld();
    	beginTime();
    }
    
    /**
     * Starts timed events such as player and NPC movement.
     */
    private void beginTime()
    {
    	//This pacer controls how fast or slow the player is
    	//able to move
    	Counter movementPacer = new Counter(5);
    	
    	//Calls render repeatedly
    	AnimationTimer animator = new AnimationTimer() 
    	{
			@Override
			public void handle(long now)
			{
				handleMovement(movementPacer);
				watchNextLevel();
				render();
			}
    	};
    	
    	animator.start();
    }
    
    /**
     * Checks to see if the player is ready to enter the next level
     * Increments the player's level and creates a new dungeon
     */
    private void watchNextLevel()
    {
    	if (view.getPlayerLevel() != view.hero().getLevel())
    	{
    		view.setPlayerLevel(view.getPlayerLevel()+1);
    		createWorld();
		world.add(new LevelUpScreen(view));
    	}
    }
    
    /**
     * Renders all objects in the world array list using their 
     * {@code draw()} methods
     */
    private void render()
    {
    	for (GameObject go : world)
    	{
    		go.draw();
    	}
    }
    
    /**
     * Handles the movement of the player
     * @param pacer Controls how fast the player is able to move around the screen.
     */
    private void handleMovement(Counter pacer)
    {
    	//Slows down movement
    	if (pacer.ready())
    	{
    		//Player movement
        	if (direction.contains(Direction.UP))
        	{
        		view.hero().moveUp();
        	}
        	if (direction.contains(Direction.DOWN))
        	{
        		view.hero().moveDown();
        	}
        	if (direction.contains(Direction.LEFT))
        	{
        		view.hero().moveLeft();
        	}
        	if (direction.contains(Direction.RIGHT))
        	{
        		view.hero().moveRight();
        	}
        	
        	
        	//Tells intelligent beings (enemies) to move
        	for (int i = 0; i < view.world().size(); i++)
        	{
        		GameObject go = view.world().get(i);
        		if (go instanceof Intelligent)
        		{
        			Intelligent m = (Intelligent) go;
        			m.ai();
        		}
        	}
        	
        	//Removes exploded bombs
        	for (int i = 0; i < view.world().size(); i++)
        	{
        		GameObject go = view.world().get(i);
        		if (go instanceof Bomb)
        		{
        			Bomb b = (Bomb) go;
        			if (b.hasExploaded())
        			{
        				view.world().remove(i);
        				i--;
        			}
        		}
        	}
        	
        	//Checks to see if the game is over
        	if (view.hero().isGameOver())
        	{
        		//Removes every object
        		while (view.world().size() > 0)
        		{
        			view.world().remove(0);
        		}
        		
        		view.world().add(new GameOverScreen(view));
        	}
    	}
    }
    
    /**
     * Performs the initial setup for a dungeon. 
     * Called after JavaFX has setup a new window.
     */
    private void createWorld()
    {
    	view.setWorld(world);
    	
    	Background bg = new Background(view);
    	world.add(bg);
       	view.setBackground(bg);
       	
    	Dungeon dungeon = new Dungeon(view);
    	world.add(dungeon);
    	view.setDungeon(dungeon);
    	
    	Player hero = new Player(view);
    	world.add(hero);
    	view.setHero(hero);
    	
    	Message msg = new Message(view);
    	world.add(msg);
    	view.setMsg(msg);
    	
    	view.msg().setMessage("Hello Adventurer! Welcome To Level " + view.getPlayerLevel());
    	view.msg().showMessage();
    	
    	System.out.println("World Finished");
    	
    	int numOfEnemies = 900 + view.getPlayerLevel()*450;
    	for (int i = 0; i < numOfEnemies; i++)
    	{
    		world.add(new Enemy(view));
    	}
    	
    	view.hero().updateView();
    }
    
    /**
     * A helper method to ensure that all direction key strokes of 
     * type Direction are completely removed from the array as 
     * duplicates were sometimes added with KeyPressed events leading 
     * to an uncontrollable player.
     * @param dir The direction to be completely removed from {@code list}
     * @param list The list that {@code dir} is to be removed from
     */
    private void removeAllFromArray(Direction dir, ArrayList<Direction> list)
    {
    	while (list.contains(dir))
    	{
    		list.remove(dir);
    	}
    }

    /**
     * Sets up key listeners to allow player movement on the screen.
     * @param stage The stage being utilized by JavaFX
     */
    private void listen(Stage stage)
    {
    	/**
    	 * Adds the direction to a list to allow multiple directions
    	 * simultaneously. The list is used in handleMovement().
    	 */
		stage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>()
		{
			@Override
			public void handle(KeyEvent e)
			{
				KeyCode c = e.getCode();		
				if ((c.equals(KeyCode.UP) || c.equals(KeyCode.W)) && view.getY() > 0)
				{
					direction.add(Direction.UP);
				}
				if ((c.equals(KeyCode.DOWN) || c.equals(KeyCode.S)) && view.getY() + view.getHeight() < view.getWorldHeight())
				{
					direction.add(Direction.DOWN);
				}
				if ((c.equals(KeyCode.RIGHT) || c.equals(KeyCode.D)) && view.getX() + view.getWidth() < view.getWorldWidth())
				{
					direction.add(Direction.RIGHT);
				}
				if ((c.equals(KeyCode.LEFT) || c.equals(KeyCode.A)) && view.getX() > 0)
				{
					direction.add(Direction.LEFT);
				}
			}
		});
		
		/**
		 * Removes the direction on key release. It uses the helper method 
		 * removeAllFromArray() as the duplicate directions were being added 
		 * to the list by the above Event Handler.
		 */
		stage.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>()
		{
			@Override
			public void handle(KeyEvent e)
			{
				KeyCode c = e.getCode();		
				if ((c.equals(KeyCode.UP) || c.equals(KeyCode.W)) && view.getY() > 0)
				{
					removeAllFromArray(Direction.UP, direction);
				}
				if ((c.equals(KeyCode.DOWN) || c.equals(KeyCode.S)) && view.getY() + view.getHeight() < view.getWorldHeight())
				{
					removeAllFromArray(Direction.DOWN, direction);
				}
				if ((c.equals(KeyCode.RIGHT) || c.equals(KeyCode.D)) && view.getX() + view.getWidth() < view.getWorldWidth())
				{
					removeAllFromArray(Direction.RIGHT, direction);
				}
				if ((c.equals(KeyCode.LEFT) || c.equals(KeyCode.A)) && view.getX() > 0)
				{
					removeAllFromArray(Direction.LEFT, direction);
				}
			}
		});
		
		//Sets up keys for player actions besides movement
		stage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>()
		{
			@Override
			public void handle(KeyEvent e)
			{
				KeyCode c = e.getCode();
				
				//Use space for a bomb
				if (c.equals(KeyCode.SPACE))
				{
					view.msg().hideMessage();
				}
			}
		});
		
		stage.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>()
		{
			@Override
			public void handle(MouseEvent e)
			{
				if (e.isPrimaryButtonDown())
				{
					view.hero().attack();
				}
				if (e.isSecondaryButtonDown())
				{
					view.world().add(new Bomb(view));
				}
			}
		});
		
    	
//		stage.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>()
//		{
//			@Override
//			public void handle(MouseEvent e)
//			{
//				if (e.isPrimaryButtonDown())
//				{
//					if ((e.getSceneY() < (view.getWindowHeight() / 2)) && 
//						view.getY() > 0)
//					{
//						hero.moveUp();
//					}
//					if ((e.getSceneY() > (view.getWindowHeight() / 2)) && 
//						view.getY() + view.getHeight() < view.getWorldHeight())
//					{
//						hero.moveDown();
//					}
//					if ((e.getSceneX() < (view.getWindowWidth() / 2)) && 
//						view.getX() + view.getWidth() < view.getWorldWidth())
//					{
//						hero.moveLeft();
//					}
//					if ((e.getSceneX() > (view.getWindowWidth() / 2)) && 
//						view.getX() > 0)
//					{
//						hero.moveRight();
//					}
//				}
//			}
//		});
		
//		stage.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>()
//		{
//			@Override
//			public void handle(KeyEvent e)
//			{
//				//System.out.println(e.getCode());
//				KeyCode c = e.getCode();		
//				if (c.equals(KeyCode.UP) && view.getY() > 0)
//				{
//					hero.moveUp();
//				}
//				if (c.equals(KeyCode.DOWN) && view.getY() + view.getHeight() < view.getWorldHeight())
//				{
//					hero.moveDown();
//				}
//				if (c.equals(KeyCode.RIGHT) && view.getX() + view.getWidth() < view.getWorldWidth())
//				{
//					hero.moveRight();
//				}
//				if (c.equals(KeyCode.LEFT) && view.getX() > 0)
//				{
//					hero.moveLeft();
//				}
//			}
//		});
    }
    
//    // Clear away portions as the user drags the mouse
//    canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, 
//    new EventHandler<MouseEvent>() {
//        @Override
//        public void handle(MouseEvent e) {
//            gc.clearRect(e.getX() - 2, e.getY() - 2, 5, 5);
//        }
//    });
}
//https://docs.oracle.com/javase/8/javafx/graphics-tutorial/canvas.htm#JFXGR214
