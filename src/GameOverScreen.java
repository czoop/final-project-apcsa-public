import com.sun.javafx.css.FontFace;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class GameOverScreen extends GameObject 
{
	/**
	 * Creates a new Game Over Screen
	 * @param view The view controller for the game
	 */
	public GameOverScreen(View view) 
	{
		super(view);
	}
	
	/**
	 * Draws the Game Over Screen using the "Lato Bold" font
	 */
	@Override
	public void draw()
	{
		view.gc().setFill(Color.BLACK);
		view.gc().fillRect(0, 0, view.getWindowWidth(), view.getWindowHeight());
		
		view.gc().setTextAlign(TextAlignment.CENTER);
		view.gc().setFont(new Font("Lato Bold", 72));
		view.gc().setFill(Color.WHITESMOKE);
		view.gc().fillText("Game Over", view.getWindowWidth()/2 - 10,
										  view.getWindowHeight()/2 - 10);
	}
}
