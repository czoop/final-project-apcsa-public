/**
 * Requires objects to have an ai() method. 
 * The ai() method is used to perform certain tasks every frame
 * @author Connor Zwick
 */
public interface Intelligent {
	void ai();
}
