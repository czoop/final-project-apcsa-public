import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;

public class View 
{
	private int x;
	private int y;
	
	private int width;
	private int height;
	
	private int worldWidth;
	private int worldHeight;
	
	private int windowWidth;
	private int windowHeight;
	
	private int scale;
	
	private int playerLevel;
	
	private GraphicsContext gc;
	
	private Image sprites;
	private Image walls;

	//References
	private ArrayList<GameObject> world;
	private Background background;
	private Dungeon dungeon;
	private Player hero;
	private Message msg;
	
	/**
	* Creates a new View controller for the Game Master
	* The View object contains refferences to every important
	* object in the game and keeps track of all location/view
	* related variables. Every GameObject requires the view
	* controller.
	*/
	public View(int x, int y, int width, int height, int worldWidth, int worldHeight, int windowWidth, int windowHeight, GraphicsContext gc)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.worldWidth = worldWidth;
		this.worldHeight = worldHeight;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		this.gc = gc;
		
		this.playerLevel = 0;
		
		this.sprites = new Image("sprites/sprites.png");
		this.walls = new Image("sprites/walls.png");
		
		//this.sprites = new Image("file:///Users/kids/git/final-project-czoop/src/sprites/sprites.png");
		//this.walls = new Image("file:///Users/kids/git/final-project-czoop/src/sprites/walls.png");
		
		//this.scale = worldWidth / width;
		//this.scale = (width / worldWidth);
		this.scale = windowWidth / width;
		
		System.out.println(Font.getFontNames());
	}
	
	/**
	* @return Returns the Player's Level
	*/
	public int getPlayerLevel()
	{
		return this.playerLevel;
	}
	
	/**
	* Sets the player's level
	*/
	public void setPlayerLevel(int val)
	{
		this.playerLevel = val;
	}

	/**
	 * Gives the canvas' graphics controller
	 * @return The graphics controller for the canvas
	 * allowing objects to render themselves in their
	 * draw methods
	 */
	public GraphicsContext gc() 
	{
		return gc;
	}
	
	/**
	 * Returns the image with the various sprites on it
	 * @return An Image that contains various sprites
	 */
	public Image getSprites()
	{
		return this.sprites;
	}
	
	/**
	 * Returns the image for the walls of the dungeon
	 * @return An Image containing the sprites for the
	 * dungeon walls
	 */
	public Image getWalls()
	{
		return this.walls;
	}
	
	/**
	 * Gives access to the message in the game
	 * @return The Message displayed to the player
	 * in the game
	 */
	public Message msg() 
	{
		return msg;
	}

	/**
	 * Sets the message OBJECT displayed to the player
	 * DOES NOT change the message content. Access the 
	 * message object directly to change the message's text
	 * @param msg The new message object to be used
	 */
	public void setMsg(Message msg) 
	{
		this.msg = msg;
	}

	/**
	 * Returns the world arraylist which dictates what objects
	 * appear on screen
	 * @return
	 */
	public ArrayList<GameObject> world() 
	{
		return world;
	}

	/**
	 * Returns the background object rendered in the world
	 * @return The background object showed in the view
	 */
	public Background background() 
	{
		return background;
	}

	/**
	 * Returns the dungeon containing the locations 
	 * of walkable areas for a player in a xy plane
	 * @return The dungeon object used by the game master
	 */
	public Dungeon dungeon() 
	{
		return dungeon;
	}

	/**
	 * Returns the hero object that the player controls
	 * during the game.
	 * @return The hero object used by the game
	 */
	public Player hero() 
	{
		return hero;
	}

	/**
	 * Sets the world array list used by the game
	 * @param world The array list to be used
	 */
	public void setWorld(ArrayList<GameObject> world)
	{
		this.world = world;
	}

	/**
	 * Sets the background object used by the game
	 * @param background The new background object
	 * to be used
	 */
	public void setBackground(Background background) 
	{
		this.background = background;
	}

	/**
	 * Sets the dungeon object to be used by the game
	 * @param dungeon The new dungeon object to be used
	 */
	public void setDungeon(Dungeon dungeon) 
	{
		this.dungeon = dungeon;
	}

	/**
	 * Sets the hero or player object to be controlled
	 * by the human player
	 * @param hero The new player object to be used
	 */
	public void setHero(Player hero) 
	{
		this.hero = hero;
	}
	
	/**
	 * Checks whether the given x and y coordinates are within
	 * the bounds of the dungeon
	 * @param x The x coordinate to check
	 * @param y The y coordinate to check
	 * @return True if the coordinates are within the bounds 
	 * of the dungeon, false otherwise
	 */
	public boolean inBounds(int x, int y)
	{
		if (x < this.worldWidth && x > 0 &&
			y < this.worldHeight && y > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Moves the VIEW up
	 */
	public void moveUp()
	{
		y--;
	}
	
	/**
	 * Moves the VIEW down
	 */
	public void moveDown()
	{
		y++;
	}
	
	/**
	 * Moves the VIEW left
	 */
	public void moveLeft()
	{
		x--;
	}
	
	/**
	 * Moves the VIEW right
	 */
	public void moveRight()
	{
		x++;
	}
	
	/**
	 * Gives the width of the dungeon
	 * @return The width of the dungeon
	 */
	public int getWorldWidth() 
	{
		return worldWidth;
	}

	/**
	 * Sets the value the view uses to check bounds
	 * for the dungeon's width. DOES NOT CHANGE THE
	 * ACTUAL DUNGEON!
	 * @param worldWidth The new width to be used for
	 * bounds checks
	 */
	public void setWorldWidth(int worldWidth) 
	{
		this.worldWidth = worldWidth;
	}

	/**
	 * Returns the height of the dungeon
	 * @return The height of the dungeon
	 */
	public int getWorldHeight() 
	{
		return worldHeight;
	}

	/**
	 * Sets the height of the dungeon used for bounds
	 * checks. DOES NOT CHANGE THE ACTUAL DUNGEON!
	 * @param worldHeight The new dungeon height to be used
	 */
	public void setWorldHeight(int worldHeight) 
	{
		this.worldHeight = worldHeight;
	}
	
	/**
	 * Returns the width of the window used to display the game
	 * @return The width of the window
	 */
	public int getWindowWidth() {
		return windowWidth;
	}

	/**
	 * Returns the height of the window used to display the game
	 * @return The height of the window
	 */
	public int getWindowHeight() {
		return windowHeight;
	}

	/**
	 * Sets the window width referenced by the view controller
	 * DOES NOT CHANGE THE WINDOW'S WIDTH!
	 * @param windowWidth The new window width for the view
	 * controller to reference
	 */
	public void setWindowWidth(int windowWidth) {
		this.windowWidth = windowWidth;
	}

	/**
	 * Sets the window height referenced by the view controller
	 * DOES NOT CHANGE THE WINDOW'S HEIGHT!
	 * @param windowHeight The new window height for the view
	 * controller to reference
	 */
	public void setWindowHeight(int windowHeight) {
		this.windowHeight = windowHeight;
	}

	/**
	 * Sets the gc object used by Game Objects to render
	 * @param gc The new GraphicsContect object to be used
	 */
	public void setGc(GraphicsContext gc) 
	{
		this.gc = gc;
	}

	/**
	 * @return The current x position of the VIEW (Top Left Corner)
	 */
	public int getX() 
	{
		return x;
	}

	/**
	 * @return The current y position of the VIEW (Top Left Corner)
	 */
	public int getY() 
	{
		return y;
	}

	/**
	 * Returns the width of the view, this determines the level
	 * of zoom when viewing the dungeon. A width of 100 indicates that
	 * the width will be 100 blocks of the dungeon wide
	 * @return The width used by the view controller
	 */
	public int getWidth() 
	{
		return width;
	}

	/**
	 * Returns the height of the view, this determines the level
	 * of zoom when viewing the dungeon. A height of 100 indicates that
	 * the height will be 100 blocks of the dungeon tall
	 * @return The height used by the view controller
	 */
	public int getHeight() 
	{
		return height;
	}

	/**
	 * Sets the x position of the view controller (Top Left Corner)
	 * @param x The x position to be used
	 */
	public void setX(int x) 
	{
		this.x = x;
	}

	/**
	 * Sets the y position of the view controller (Top Left Corner)
	 * @param y The y position to be used
	 */
	public void setY(int y) 
	{
		this.y = y;
	}

	/**
	 * Sets the width of the view to determine the level of zoom
	 * @param width The new width to be used by the view controller
	 */
	public void setWidth(int width) 
	{
		this.width = width;
	}

	/**
	 * Sets the height of the view to determine the level of zoom
	 * @param height The new height to be used by the view controller
	 */
	public void setHeight(int height) 
	{
		this.height = height;
	}
	
	/**
	 * Returns a multiplier used to properly scale the displayed
	 * blocks of the dungeon along with sprites. It is determined
	 * by the given width and height of the view.
	 * @return The scale factor to be used during renders
	 */
	public int getScale() 
	{
		return scale;
	}
}
