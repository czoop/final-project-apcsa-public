import javafx.scene.paint.Color;

public class Bomb extends GameObject implements Intelligent
{
	private Counter counter;
	private boolean hasExploaded;
	private int radius;
	
	/**
	* Creates a new bomb object
	* @param view The view controller for the game
	*/
	public Bomb(View view) 
	{
		super(view);
		this.counter = new Counter(20);
		this.x = view.hero().getX();
		this.y = view.hero().getY();
		this.radius = 0;
		this.hasExploaded = false;
	}
	
	/**
	* Causes the bomb to expload. Destroys the blocks around the bomb.
	* Kills all nearby enemies and hurts the player.
	*/
	public void blow() 
	{
		if (counter.ready() && !hasExploaded)
		{
			for (int r = y - 2; r <= y + 2; r++)
			{
				for (int c = x - 2; c <= x + 2; c++)
				{
					 view.dungeon().setState(c, r, false);
				}
			}
			
			//Damages the player if they are too close
			if (this.distanceFrom(view.hero()) < 5)
			{
				view.hero().adjustLife(-5);
			}
			
			//Goes through the enemies to hurt them
			for (int i = 0; i < view.world().size(); i++)
			{
				GameObject go = view.world().get(i);
				if (go instanceof Enemy)
				{
					if (go.distanceFrom(this) < 5)
					{
						view.world().remove(i);
						i--;
					}
				}
			}
			this.hasExploaded = true;
		}
	}
	
	/**
	* Checks to see if the bomb is ready to blow.
	*/
	@Override
	public void ai() 
	{
		blow();
	}
	
	/**
	* Signals the game master to remove the bomb from the world
	* @return true if the bomb has exploaded, false otherwise.
	*/
	public boolean hasExploaded()
	{
		return hasExploaded;
	}
	
	/**
	* Draws the bomb object including the expanding circle
	*/
	@Override
	public void draw()
	{
		double scale = view.getScale();
		double dx = (x - view.getX()) * scale;
		double dy = (y - view.getY()) * scale;
		view.gc().setFill(Color.rgb(255, 0, 0, 0.5));
		view.gc().fillOval(dx-(radius/2)+3, dy-(radius/2)+3, radius, radius);
		radius++;
		view.gc().drawImage(view.getSprites(), 240, 48, 16, 16, dx, dy, scale, scale);
	}
}
