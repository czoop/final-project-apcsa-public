import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Player extends GameObject implements Intelligent
{
	protected Dungeon dungeon;
	protected Direction dir;
	protected int lives;
	protected boolean facingRight;
	protected boolean swordShowing;
	protected int swordShowCount;
	protected int xp;
	protected int level;
	
	/**
	 * Creates a new player object for a dungeon
	 * @param view The view/state controller of the game
	 * @param dungeon The dungeon in which the player will be acting
	 */
	public Player(View view)
	{
		super(view);
		this.dungeon = view.dungeon();
		this.dir = Direction.UP;
		this.lives = 100;
		this.facingRight = true;
		this.swordShowing = false;
		this.xp = 0;
		swordShowCount = 0;
		this.level = view.getPlayerLevel();
		choosePosition();
		//updateView();
	}
	
	/**
	* Get the current Game Level, may momentarialy be 
	* different from the Player Level
	* @return The player's level
	*/
	public int getLevel()
	{
		return this.level;
	}
	
	/**
	* Sets the current player level
	* @param val The value to change the player level to
	*/
	public void setLevel(int val)
	{
		this.level = val;
	}
	
	/**
	* Increases the player level by a given amount,
	* DOES NOT SET THE LEVEL
	* @param val The amount to add to the player level
	*/
	public void addLevel(int val)
	{
		this.level += val;
	}
	
	/**
	* Adds a certain amount of XP to the player's level
	* @param num The amount to add to the player's XP
	*/
	public void addXP(int num)
	{
		this.xp += num;
	}
	
	/**
	* Alter's the player's life by a given amount.
	* @param val The amount to adjust the player's life by
	*/
	public void adjustLife(int val)
	{
		this.lives += val;
	}
	
	/**
	* Alerts the Game Master to the need for a new level
	* @return true if the player has passes 100 xp and requires a new level,
	* false otherwise
	*/
	public boolean newLevel()
	{
		return xp > 100;
	}
	
	/**
	 * Moves the player up
	 */
	public void moveUp()
	{
		move(x, y-1);
		this.dir = Direction.UP;
	}
	
	/**
	 * Moves the player down
	 */
	public void moveDown()
	{
		move(x, y+1);
		this.dir = Direction.DOWN;
	}
	
	/**
	 * Moves the player left
	 */
	public void moveLeft()
	{
		move(x-1, y);
		this.dir = Direction.LEFT;
		facingRight = false;
	}
	
	/**
	 * Moves the player right
	 */
	public void moveRight()
	{
		move(x+1, y);
		this.dir = Direction.RIGHT;
		facingRight = true;
	}
	
	/**
	 * A helper method that moves the player to given coordinates if the coordinates are open
	 * @param x The x position the player should be moved to
	 * @param y The y position the player should be moved to
	 */
	protected void move(int x, int y)
	{
		if (dungeon.isValidPosition(x, y))
		{
			this.x = x;
			this.y = y;
		}
		updateView();
	}
	
	/**
	 * Re-centers the view so that the player is in the center of the screen
	 */
	public void updateView()
	{
		view.setX(x - view.getWidth() / 2);
		view.setY(y - view.getHeight() / 2);
		//System.out.println("Player: " + x + " " + y);
	}
	
	/**
	 * Chooses the initial position for the player randomly
	 */
	private void choosePosition()
	{
		while (!dungeon.isValidPosition(x, y))
		{
			x = (int) (Math.random()*view.getWorldWidth());
			y = (int) (Math.random()*view.getWorldHeight());
		}
	}
	
	/**
	* Causes damage to the monsters arround the player,
	* also causes the sword to show for a short amount of time
	* by adjusting swordShowCount
	*/
	public void attack()
	{
		swordShowCount = 14;
		for (int i = 0; i < view.world().size(); i++)
		{
			if (view.world().get(i) instanceof Enemy)
			{
				Enemy e = (Enemy) view.world().get(i);
				if (e.distanceFrom(this) < 2)
				{
					//e.slash causes damage on call
					if (e.slash())
					{
						//Enemy is dead
						view.world().remove(i);
						i--;
						xp += 10;
					}
				}
			}
		}
	}
	
	/**
	* Causes damage to the player if enemies are too close
	*/
	@Override 
	public void ai()
	{
		//Hurts the player if enemies are too close
		for (int i = 0; i < view.world().size(); i++)
		{
			if (view.world().get(i) instanceof Enemy)
			{
				if (view.world().get(i).distanceFrom(this) < 2)
				{
					lives--;
				}
			}
		}
		
		//Restores the player's life if their XP is equal to 100
		if (this.xp >= 100)
		{
			this.lives = 100;
			this.xp = 0;
			this.level++;
		}
	}
	this.levelUpScreen = new LevelUpScreen(this);
	/**
	* Tells the Game Master if the game is over (if the player has no life left)
	* @return true if the game is over, false otherwise
	*/
	public boolean isGameOver()
	{
		return lives <= 0;
	}
	
//	@Override
//	public void draw()
//	{
//		view.gc().setFill(Color.BISQUE);
//		view.gc().fillRect((x - view.getX())* view.getScale(), 
//						   (y - view.getY()) * view.getScale(), 
//						   view.getScale(), 
//						   view.getScale());
//	}
	
	/**
	* Draws the player, sword, health bar, and XP bar on screen
	*/
	@Override 
	public void draw()
	{
		double scale = view.getScale();
		double dx = (x - view.getX()) * scale;
		double dy = (y - view.getY()) * scale;
		if (facingRight)
		{
			view.gc().drawImage(view.getSprites(), 240, 230, 16.0, 26.0, dx, dy - 10, scale, scale + 10);
		}
		else //FacingLeft
		{
			view.gc().drawImage(view.getSprites(), 240 + 16, 230, -16.0, 26.0, dx, dy - 10, scale, scale + 10);
		}
		
		//Health Bar
		view.gc().setFill(Color.DARKSLATEGRAY);
		view.gc().fillRect(view.getWindowWidth() - 110, 10, 100, 25);
		view.gc().setFill(Color.RED);
		view.gc().fillRect(view.getWindowWidth() - 110, 10, lives, 25);
		view.gc().fillText("Health", view.getWindowWidth() - 170, 27);
		
		//XP Bar
		view.gc().setFill(Color.DARKSLATEGRAY);
		view.gc().fillRect(view.getWindowWidth() - 110, 35, 100, 25);
		view.gc().setFill(Color.PALEGREEN);
		view.gc().fillRect(view.getWindowWidth() - 110, 35, xp, 25);
		view.gc().fillText("XP", view.getWindowWidth() - 145, 53);
		
		//Sword
		if (swordShowCount > 0)
		{
			swordShowCount--;
			swordShowing = true;
		}
		else
		{
			swordShowing = false;
		}
		
		if (swordShowing && facingRight)
		{
			view.gc().drawImage(view.getSprites(), 192, 32, 16, 30, dx+7, dy-5, 16, 20);
		}
		else if (swordShowing && !facingRight)
		{
			view.gc().drawImage(view.getSprites(), 192, 32, 16, 30, dx-7, dy-5, 16, 20);
		}
	}
}
