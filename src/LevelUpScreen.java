import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class LevelUpScreen extends GameObject implements Intelligent
{
	private double counter;
	
	/**
	* Creates a new Level Up Screen
	* @param view The view controller for the game
	*/
	public LevelUpScreen(View view)
	{
		super(view);
		this.counter = 1.0;
	}
  
  	/**
	* Causes the screens opacity to slowly lower
	*/
  	@Override
	public void ai()
	{
		if (counter > 0)
		{
			counter -= 0.02;
		}
		if (counter < 0)
		{
			counter = 0;
		}
  	}
	
	/**
	* Renders the screen on the game
	*/
	@Override
	public void draw()
	{
		Font defaultFont = view.gc().getFont();
		view.gc().setFill(Color.rgb(0, 0, 0, counter));
		view.gc().fillRect(0, 0, view.getWindowWidth(), view.getWindowHeight());
		view.gc().setTextAlign(TextAlignment.CENTER);
		view.gc().setFont(new Font("Lato Bold", 72));
		view.gc().setFill(Color.rgb(250, 250, 250, counter));
		view.gc().fillText("Level " + view.getPlayerLevel(), view.getWindowWidth()/2 - 10,
										  view.getWindowHeight()/2 - 10);
		
		view.gc().setTextAlign(TextAlignment.LEFT);
		view.gc().setFont(defaultFont);
	}
}
