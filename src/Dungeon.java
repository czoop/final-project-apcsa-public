import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class Dungeon extends GameObject
{
	private boolean[][] dungeon;
	
	
	//Good Settings: initialChance: 0.6, birthLim: 6, deathLim: 4
	/**
	 * Generates a new dungeon
	 * @param view The view/state controller for the game
	 */
	public Dungeon (View view)
	{
		super(view);
		this.dungeon = new boolean[view.getWorldHeight()][view.getWorldWidth()];
		generate(0.6);
		
		for (int i = 0; i < 10; i++)
		{
			//birthLim, deathLim
			smooth(6, 4);
		}
		//System.out.println(floodFill());
	}
	
	/**
	 * Checks if the given x and y position to see if it's a movable position or a wall
	 * @param x The x position to be checked
	 * @param y The y position to be checked
	 * @return True if the position is a valid position for a player, False otherwise
	 */
	public boolean isValidPosition(int x, int y)
	{
		if (view.inBounds(x, y))
		{
			return dungeon[y][x];
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Changes the state of a block at the given position to the provided value
	 * @param x
	 * @param y
	 * @param solid
	 */
	public void setState(int x, int y, boolean solid)
	{
		if (view.inBounds(x, y))
		{
			dungeon[y][x] = !solid;
		}
	}
	
	/**
	 * Generates the random map seeding the dungeon, to be used before smooth()
	 * @param initialChance The probability that a given cell will start alive
	 */
	public void generate(double initialChance)
	{
		if (initialChance > 1 || initialChance < 0)
		{
			initialChance = 0.6;
		}
		
		//Creates a completely random map
		for (int r = 0; r < dungeon.length; r++)
		{
			for (int c = 0; c < dungeon[r].length; c++)
			{
				if (Math.random() < initialChance)
				{
					dungeon[r][c] = true;
				}
			}
		}
		clearEdges();
	}
	
	/**
	 * Clears the edges of the map to prevent the player from leaving. 
	 * It creates large margins so that the entire dungeon is explorable
	 * rather than just viewable (as the view cannot extend out of the 
	 * dungeon's range)
	 */
	private void clearEdges()
	{
		//Sets the edges of the map to be walls
		for (int r = 0; r < dungeon.length; r++)
		{
			for (int width = 0; width < view.getWidth(); width++)
			{
				dungeon[r][width] = false;
				dungeon[r][dungeon[r].length - 1 - width] = false;
			}
		}
		
		for (int c = 0; c < dungeon[0].length; c++)
		{
			for (int height = 0; height < view.getHeight(); height++)
			{
				dungeon[height][c] = false;
				dungeon[dungeon.length - 1 - height][c] = false;
			}
		}
	}
	
	/**
	 * Smooths the dungeon making it possible to navigate, as opposed to the random static
	 * created my generate()
	 * @param birthLimit The number of friends a dead cell needs to become alive
	 * @param deathLimit The number of friends an alive cell needs to stay alive
	 */
	private void smooth(int birthLimit, int deathLimit)
	{
		boolean[][] newMap = new boolean[view.getWorldHeight()][view.getWorldWidth()];
		for (int r = 1; r + 1 < dungeon.length; r++)
		{
			for (int c = 1; c + 1 < dungeon[r].length; c++)
			{
				int friends = countAlive(r, c);
				boolean isAlive = dungeon[r][c];
				
				if (isAlive && friends >= deathLimit)
				{
					newMap[r][c] = true;
				}

				else if (!isAlive && friends >= birthLimit)
				{
					newMap[r][c] = true;
				}
			}
		}
		dungeon = newMap;
	}
	
	/**
	 * Counts the number of alive (true) cells surrounding a given cell at
	 * position x, y
	 * @param row The y position of the cell
	 * @param col The x position of the cell
	 * @return The number of true cells surrounding the given cell, 
	 * not including the cell itself
	 */
	private int countAlive(int row, int col)
	{
		int count = 0;
		for (int r = row - 1; r <= row + 1; r++)
		{
			for (int c = col - 1; c <= col + 1; c++)
			{
				if (dungeon[r][c])
				{
					count++;
				}
			}
		}
		
		//Removes one as we shouldn't count ourselves
		if (dungeon[row][col])
		{
			count--;
		}
		
		return count;
	}
	
//	private int floodFill()
//	{
//		int x = 0;
//		int y = 0;
//		for (int r = 0; r < view.getWorldWidth(); r++)
//		{
//			for (int c = 0; c < view.getWorldHeight(); c++)
//			{
//				if (dungeon[r][c])
//				{
//					x = r;
//					y = c;
//				}
//			}
//		}
//		return floodFill(x, y);
//	}
//	
//	private int floodFill(int x, int y)
//	{
//		return 0;
//	}
	
	/**
	 * Gives the view of the dungeon at a specific point as a string
	 * of black and white blocks. NOTE IT DOES NOT WORK PROPERLY
	 * @param x The x position of the center of the desired view in the dungeon
	 * @param y The y position of the center of the desired view in the dungeon
	 * @param width The width of the wanted view
	 * @param height The height of the wanted view
	 * @return A string of black and white blocks meant to represent the dungeon's
	 * walls according to the given parameters
	 */
	public String printView(int x, int y, int width, int height)
	{
		//Does not work properly
		String str = "";
		int hw = width / 2;
		int hh = height / 2;
		for (int c = y - hh; c <= y + hh; c++)
		{
			for (int r = x - hw; r <= x + hw; r++)
			{
				if ((r == x && c == y) || (r == y && c == x))
				{
					str += "X ";
				}
				else if (dungeon[r][c])
				{
					str += "◼ ";
				}
				else
				{
					str += "◻ ";
				}
				
				if (r == x + hw)
				{
					str += "\n";
				}
			}
		}
		return str;
	}
	
	/**
	 * Draws the dungeon and its blocks on screen
	 */
	@Override
	public void draw()
	{
//		int red = (int) (Math.random()*255);
//		int green = (int) (Math.random()*255);
//		int blue = (int) (Math.random()*255);
//		Color color = Color.rgb(red, green, blue);
//		view.gc().setFill(color);
		view.gc().setFill(Color.DIMGRAY);
		for (int r = view.getY(); r < view.getY() + view.getHeight(); r++)
		{
			for (int c = view.getX(); c < view.getX() + view.getWidth(); c++)
			{
				if (view.inBounds(r, c) && dungeon[r][c])
				{
					view.gc().fillRect((c - view.getX()) * view.getScale(), 
									   (r - view.getY()) * view.getScale(), 
									    view.getScale(),
									    view.getScale());
				}
				if (!dungeon[r-1][c] && dungeon[r][c])
				{
					//Draws the wall on the bottom
					int s = view.getScale();
					int dx = (c - view.getX()) * s;
					int dy = (r - view.getY()) * s;
					view.gc().drawImage(view.getWalls(), 16, 16, 16, 16, dx, dy, s, s);
				}
				if (!dungeon[r][c - 1] && dungeon[r][c])
				{
					//Right side of walls
					int s = view.getScale();
					int dx = (c - view.getX()) * s;
					int dy = (r - view.getY()) * s;
					view.gc().drawImage(view.getWalls(), 16, 16, 2, 16, dx-2, dy, 2, s);
				}
				if (!dungeon[r][c + 1] && dungeon[r][c]  && true)
				{
					//Left side of walls
					int s = view.getScale();
					int dx = (c - view.getX()) * s;
					int dy = (r - view.getY()) * s;
					view.gc().drawImage(view.getWalls(), 16, 16, 2, 16, dx + s, dy, 2, s);
				}
			}
		}
	}
}